package com.robocos.simulation;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

import box2dLight.Light;
import box2dLight.PointLight;
import box2dLight.RayHandler;

public class Arrow extends Sprite {

	public Vector2 initialPos, velocity, pos;
	public boolean launched;
	public float width, height, t = 0, totalFlightTime = 0, timeIncrement = 0.05f;
	public float velMax = SimWorld.toPixel(2f);
	public ArrayList<Trajectory> trajs = new ArrayList<Trajectory>();

	public int launch;
	private Light light;

	private Bow bow;

	public Arrow(SimWorld world, Bow bow) {
		super(new Texture(Gdx.files.internal("data/arrow.png")));
		this.bow = bow;
		width = bow.width / 2;
		height = bow.height / 2;

		this.setOrigin(bow.getOriginX(), bow.getOriginY());
		this.setScale(bow.getScaleX(), bow.getScaleY());
		// this.initialPos = new Vector2(bow.initialPos.x , bow.initialPos.y + bow.height / 3.5f);
		velocity = new Vector2(0, 0);
		this.initialPos = bow.pos.cpy();
		this.pos = initialPos.cpy();
		trajs.add(new Trajectory());

		light = new PointLight(world.rayHandler, 10, trajs.get(0).color, 2 * (int) Math.sqrt(this.width * this.width + this.height * this.height), 
				initialPos.x, initialPos.y);

	}

	public void update(SimWorld simWorld, float delta) {
		if (!launched) {
			this.t = 0;
			totalFlightTime = 0;
			this.initialPos.set(bow.pos);
			this.pos.set(bow.pos);
			this.setRotation(bow.getRotation());
		} else {
			if(pos.y <= simWorld.ground.y) {
				totalFlightTime = t * 20;
				trajs.add(new Trajectory());
				launch = trajs.size() - 1;
				this.velMax = SimWorld.toPixel(2f);
				simWorld.launch = false;
			}
			else {
				t += timeIncrement / 60.0f;
				if(t > timeIncrement / 60f && trajs.size() > 0 && trajs.get(launch) != null) {
					trajs.get(launch).positions.add(pos.cpy());
					trajs.get(launch).lights.add(new PointLight(simWorld.rayHandler, 10, trajs.get(launch).color, 2 * (int) Math.sqrt(this.width * this.width + this.height * this.height), 
							pos.cpy().x, pos.cpy().y));
				}
				pos.set(getX(t), getY(t));
				simWorld.updateMatrices(this.pos);
				this.setRotation((MathUtils.radiansToDegrees * MathUtils.atan2(this.getVelocity(t).y, this.getVelocity(t).x)) - 45);
			}
		}

		if(simWorld.fireworks)
			light.setPosition(pos);
	}

	public float getX(float t) {
		return velocity.x * t + initialPos.x;
	}

	public float getY(float t) {
		return 0.5f * SimWorld.GRAVITY * t * t + velocity.y * t + initialPos.y;
	}

	private Vector2 getVelocity(float t) {
		Vector2 vel = velocity.cpy();
		vel.y += SimWorld.GRAVITY * t;
		return vel;
	}

	public void render(SpriteBatch batch) {
		batch.draw(this.getTexture(), pos.x, pos.y, getOriginX(), getOriginY(), width, height, getScaleX(), getScaleY(),
				getRotation(), getRegionX(), getRegionY(), getRegionWidth(), getRegionHeight(), false, false);
	}

	public Vector2 getFinalPos() {
		Vector2 finalPos = new Vector2();
		float t = (float) Math.sqrt(-2 * initialPos.y / SimWorld.GRAVITY);
		finalPos.x = getVelocity(t).x * t;
		finalPos.y = getVelocity(t).y * t + initialPos.y;
		return finalPos;
	}

	class Trajectory {
		ArrayList<Vector2> positions = new ArrayList<Vector2>();
		ArrayList<Light> lights = new ArrayList<Light>();
		Color color = SimWorld.randColor();
		public void clearLights() {
			for(Light l: lights)
				l.remove();

			lights.clear();
		}
	}
}