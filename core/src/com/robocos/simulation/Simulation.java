package com.robocos.simulation;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;

public class Simulation extends ApplicationAdapter {
	SpriteBatch batch;
	SimWorld simWorld;
	Color sky;
	
	@Override
	public void create () {
		batch = new SpriteBatch();
		simWorld = new SimWorld(batch);
		sky = new Color(135 / 255f, 206 / 255f, 250 / 255f, 0.1f);
	}

	@Override
	public void render () {
		Color color = simWorld.fireworks ? Color.BLACK : sky;
		Gdx.gl.glClearColor(color.r, color.g, color.b, color.a);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		simWorld.update(Gdx.graphics.getDeltaTime());
		simWorld.render();
		
	}
	
	@Override
	public void dispose () {
		batch.dispose();
	}
	
	public void resize(int width, int height) {
		simWorld.cam.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		simWorld.batch.setProjectionMatrix(simWorld.cam.combined);
		simWorld.sr.setProjectionMatrix(simWorld.cam.combined);
		simWorld.rayHandler.setCombinedMatrix(simWorld.cam.combined);
	}
}
