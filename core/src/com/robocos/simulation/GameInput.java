package com.robocos.simulation;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.TimeUtils;

import box2dLight.PointLight;

public class GameInput implements InputProcessor {

	SimWorld simWorld;
	long time;
	float[] timeIncrements = {0.05f, 0.25f, 0.5f, 1.0f};
	int currentIncrement = 0;
	boolean fullscreen = true;

	public GameInput(SimWorld simWorld) {
		this.simWorld = simWorld;
	}

	@Override
	public boolean keyDown(int keycode) {
		if (keycode == Keys.S)
			simWorld.selectLocation = !simWorld.selectLocation;
		else if (keycode == Keys.R) {
			simWorld.ready = !simWorld.ready;
		}else if(keycode == Keys.SPACE) {
			simWorld.showHits = !simWorld.showHits;
		}else if(keycode == Keys.F3)
			simWorld.showControls = !simWorld.showControls;
		else if(keycode == Keys.UP) {
			simWorld.arrow.timeIncrement = timeIncrements[(currentIncrement++) % timeIncrements.length];
		}else if(keycode == Keys.C) {
			simWorld.arrow.trajs.clear();
			simWorld.arrow.launch = 0;
		}else if(keycode == Keys.RIGHT && !simWorld.ready && !simWorld.launch && !simWorld.selectLocation) {
			simWorld.arrow.velMax += SimWorld.toPixel(0.5f);
		}else if(keycode == Keys.LEFT && !simWorld.ready && !simWorld.launch && !simWorld.selectLocation) {
			if(simWorld.arrow.velMax > 0.5f)
				simWorld.arrow.velMax -= SimWorld.toPixel(0.5f);
		}else if(keycode == Keys.X) {
			simWorld.fireworks = !simWorld.fireworks;
			
			if(simWorld.fireworks) {
			Arrow arrow = simWorld.arrow;
			if(!arrow.trajs.isEmpty() && arrow.trajs.get(arrow.launch) != null)
				if(arrow.trajs.get(arrow.launch).positions.isEmpty())
					for(Vector2 pos: arrow.trajs.get(arrow.launch).positions) {
						float width = arrow.width;
						float height = arrow.height;
						arrow.trajs.get(arrow.launch).lights.add(new PointLight(simWorld.rayHandler, 10, arrow.trajs.get(arrow.launch).color, 3.5f * (int) Math.sqrt(width * width + height * height), 
								pos.cpy().x, pos.cpy().y));
					}
			}
		}
		return true;
	}

	@Override
	public boolean keyUp(int keycode) {
		if(keycode == Keys.Q && !simWorld.launch) {
			simWorld.resetCamera();
		}else if(keycode == Keys.F11) {
			fullscreen = !fullscreen;
			if(fullscreen)
				Gdx.graphics.setFullscreenMode(Gdx.graphics.getDisplayMode());
			else
				Gdx.graphics.setWindowedMode(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		}
		return true;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		Vector3 touch = new Vector3(screenX, screenY, 0);
		simWorld.cam.unproject(touch);

		if(simWorld.selectLocation) {
			simWorld.bow.pos.set(touch.x, touch.y);
			simWorld.selectLocation = false;
		}
		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		if (simWorld.ready && TimeUtils.timeSinceMillis(time) > 2000 && !simWorld.launch) {
			Arrow arrow = simWorld.arrow;
			arrow.velocity.x = MathUtils.cosDeg(arrow.getRotation() + 45) * arrow.velMax;
			arrow.velocity.y = MathUtils.sinDeg(arrow.getRotation() + 45) * arrow.velMax;
			arrow.setRotation((MathUtils.radiansToDegrees * MathUtils.atan2(arrow.velocity.y, arrow.velocity.x)));

			simWorld.ready = false;
			simWorld.launch = true;

		}
		return true;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return true;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		Vector3 touch = new Vector3(screenX, screenY, 0);
		simWorld.cam.unproject(touch);

		if (simWorld.selectLocation && touch.y > SimWorld.ground.y && !simWorld.launch) {
			simWorld.bow.pos.x = touch.x;
			simWorld.bow.pos.y = touch.y;
		} else {
			if(!simWorld.ready) {
				float angle = MathUtils.atan2(simWorld.bow.pos.y - touch.y, simWorld.bow.pos.x - touch.x) * MathUtils.radDeg;
				//			angle = MathUtils.clamp(angle, -180, 180);
				simWorld.bow.setRotation(angle);
			}
		}
		return true;
	}

	@Override
	public boolean scrolled(int amount) {
		/*simWorld.cam.zoom *= amount > 0 ? 1.05f : 0.95f;
		simWorld.updateMatrices(new Vector2(simWorld.cam.position.x, simWorld.cam.position.y));*/
		Vector3 tp = new Vector3();
		simWorld.cam.unproject(tp.set(Gdx.input.getX(), Gdx.input.getY(), 0 ));
		float px = tp.x;
		float py = tp.y;
		simWorld.cam.zoom += amount * simWorld.cam.zoom * 0.1f;
		simWorld.cam.update();

		simWorld.cam.unproject(tp.set(Gdx.input.getX(), Gdx.input.getY(), 0 ));
		simWorld.cam.position.add(px - tp.x, py- tp.y, 0);
		simWorld.cam.update();
		simWorld.batch.setProjectionMatrix(simWorld.cam.combined);
		simWorld.sr.setProjectionMatrix(simWorld.cam.combined);
		simWorld.rayHandler.setCombinedMatrix(simWorld.cam.combined);
		return true;
	}

}
