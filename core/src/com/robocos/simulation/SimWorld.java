package com.robocos.simulation;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.robocos.simulation.Arrow.Trajectory;

import box2dLight.Light;
import box2dLight.PointLight;
import box2dLight.RayHandler;

public class SimWorld<sr> {
	public static final float GRAVITY = toPixel(-9.81f);
	public static final Vector2 ground = new Vector2(Gdx.graphics.getWidth(), Gdx.graphics.getHeight() / 20f);
	public ShapeRenderer sr;
	public SpriteBatch batch;
	public OrthographicCamera cam;
	public float WIDTH, HEIGHT;

	private BitmapFont font;
	
	public boolean selectLocation, ready, launch, showHits;

	public Bow bow;
	public Arrow arrow;
	public boolean showControls, fireworks;
	
	private Light mainLight;
	
	public RayHandler rayHandler;

	public SimWorld(SpriteBatch batch) {
		//		System.out.println(Gdx.graphics.getWidth() + " ," + Gdx.graphics.getHeight());
		WIDTH = Gdx.graphics.getWidth() / 2f;
		HEIGHT = Gdx.graphics.getHeight() / 2f;
		cam = new OrthographicCamera(WIDTH, HEIGHT);
		cam.translate(WIDTH / 2, HEIGHT / 2);
		cam.update();

		this.batch = batch;
		batch.setProjectionMatrix(cam.combined);

		sr = new ShapeRenderer();
		sr.setProjectionMatrix(cam.combined);
		
		rayHandler = new RayHandler(new World(new Vector2(0, GRAVITY), false));
		rayHandler.setCombinedMatrix(cam.combined);
		rayHandler.setShadows(true);
		
		
		bow = new Bow(new Vector2(cam.viewportWidth / 2f, cam.viewportHeight / 2f), WIDTH / 7.5f, WIDTH / 7.5f);
		font = new BitmapFont(Gdx.files.internal("data/font.fnt"));
		arrow = new Arrow(this, bow);
		mainLight = new PointLight(rayHandler, 5, Color.WHITE, 4.5f * (int) Math.sqrt(WIDTH * WIDTH + HEIGHT * HEIGHT), 
				bow.pos.x - cam.viewportWidth / 2, HEIGHT);
	

		Gdx.input.setInputProcessor(new GameInput(this));
	}

	public void update(float delta) {
		arrow.update(this, delta);
		arrow.launched = launch;
	}

	public void render() {		
		
		float xpos = bow.pos.x - cam.viewportWidth / 6f;
		float ypos = bow.pos.y + HEIGHT / 2;
	
		
		sr.begin(ShapeType.Filled);
		sr.setColor(Color.GRAY);
		sr.rect(0, ground.y, 100 * cam.zoom * ground.x, -100 * cam.zoom * cam.viewportHeight);
		sr.rect(0, ground.y, -10000 * cam.zoom * ground.x, -100 * cam.zoom * cam.viewportHeight);

		if(showHits) {
			for(Trajectory traj: arrow.trajs) {
				sr.setColor(traj.color);
				for(Vector2 pos: traj.positions)
//					sr.circle(pos.x, pos.y, toPixel(0.009f) / cam.zoom);
					sr.circle(pos.x, pos.y, arrow.width / 3);
			}
		}
		sr.end();
		
		batch.begin();
		bow.render(batch);
		if(!fireworks)
			arrow.render(batch);

		font.setColor(Color.WHITE);
		if(!showControls) {
			if(selectLocation)  {
				font.draw(batch, "Click to select inital position", xpos, ypos - 30);
				displayCoords(xpos, ypos);
			}else {
				if(ready)
					font.draw(batch, "Ready to launch", xpos, ypos - 60);
				else {
					displayCoords(xpos, ypos);
					font.draw(batch, "Current Rotation: " + (int) (arrow.getRotation() + 45) + " degrees", xpos, ypos - 120);
					font.draw(batch, "Initial Velocity: " + String.format("%.2f", toMeter(arrow.velMax)) + "m/s", xpos, ypos - 150);
					//				font.draw(batch, "Predicted Range: " + arrow.getRange(), cam.viewportWidth / 20, ypos - 150);
					font.draw(batch, "Simulation Speed: " + (arrow.timeIncrement * 2) + "x", xpos, ypos - 240);
				}
			}

		}else {
			font.draw(batch, "S: Move bow" , xpos, ypos - 30);
			font.draw(batch, "R: Ready to Launch" , xpos, ypos - 60);
			font.draw(batch, "Left Click: Launch the projectile(when ready) or select init position" , xpos, ypos - 90);
			font.draw(batch, "Q: Adjust camera" , xpos, ypos - 120);
			font.draw(batch, "Space: Show previous trajectories" , xpos, ypos - 150);
			font.draw(batch, "C: Clear previous trajectories" , xpos, ypos - 180);
			font.draw(batch, "Up arrow: Change simulation speed", xpos, ypos - 210);
			font.draw(batch, "Left/Right arrow: Change initial velocity", xpos, ypos - 240);
			font.draw(batch, "F11: Change display mode", xpos, ypos - 270);
			font.draw(batch, "Scroll wheel: Zoom into/out of point" , xpos, ypos - 300);

		}


		font.setColor(Color.RED);
		font.draw(batch, "2D Projectile Simulator", xpos, ypos +90);
		font.draw(batch, "Created by Waleed Ghazal, October 2016", xpos, ypos +60);
		font.draw(batch, showControls ? "Press F3 to return" : "Press F3 to show controls", xpos, ypos +30);
		

		batch.end();
		
		if(fireworks)  {
			mainLight.setPosition(bow.pos.x - cam.viewportWidth / 2, ypos + HEIGHT / 3);
			rayHandler.updateAndRender();
		}
	}



	/*private Vector2 getFurthestRange() {
		if(hits.size() > 0) {
			Vector2 furthest = hits.get(0);
			for(Vector2 v: hits)
				if(v.x > furthest.x)
					furthest = v.cpy();
			return furthest;
		}
		return null;
	}*/

	private void displayCoords(float xpos, float ypos) {
		
		String posX = String.format("%.2f", toMeter(arrow.pos.x));
		String posY = String.format("%.2f", toMeter(arrow.pos.y));
		
		String dx = "X: " + posX + "m";
		String dy = "Y: " + posY + "m";
		if(toMeter(arrow.pos.x) < 1)
			dx = "X: " + (String.format("%.2f", toMeter(arrow.pos.x) * 100)) + "cm";
		if(toMeter(arrow.pos.y) < 1)
			dy = "Y: " + (String.format("%.2f", toMeter(arrow.pos.y) * 100)) + "cm";
		font.draw(batch, dx, xpos, ypos - 60);
		font.draw(batch, dy, xpos, ypos - 90);

	}

	public static float toPixel(float meter) {
		return meter * 3779.527559055f;
	}

	public static float toMeter(float pixel) {
		return pixel / 3779.527559055f;
	}

	public void updateMatrices(Vector2 pos) {
		if(pos.x == WIDTH / 2) {
			cam.position.set(WIDTH / 4, WIDTH / 4, 0);
			cam.update();
			sr.setProjectionMatrix(cam.combined);
			batch.setProjectionMatrix(cam.combined);
			rayHandler.setCombinedMatrix(cam.combined);

			//			bow.pos.set(40, this.bow.height / 2 + ground.y);
			launch = false;
		}else {
			cam.position.set(pos.x, pos.y, 0);
			cam.update();
			sr.setProjectionMatrix(cam.combined);
			batch.setProjectionMatrix(cam.combined);
			rayHandler.setCombinedMatrix(cam.combined);
		}
	}

	public void resetCamera() {
		cam.zoom = 1;
		cam.position.set(bow.pos.x * 4, bow.pos.y * 4, 0);
		cam.update();
		batch.setProjectionMatrix(cam.combined);
		sr.setProjectionMatrix(cam.combined);
		rayHandler.setCombinedMatrix(cam.combined);


		//		bow.pos.set(cam.viewportWidth / 2f, cam.viewportHeight / 2f);

		cam.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		batch.setProjectionMatrix(cam.combined);
		sr.setProjectionMatrix(cam.combined);
		rayHandler.setCombinedMatrix(cam.combined);
		// TODO Auto-generated method stub

	}


	public static Color randColor() {
		return new Color(MathUtils.random(0, 255) / 255f, MathUtils.random(0, 255) / 255f, MathUtils.random(0, 255) / 255f, 1.0f);
	}
}
