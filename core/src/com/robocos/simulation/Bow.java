package com.robocos.simulation;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class Bow extends Sprite {

	public float width, height;
	public Vector2 pos;
	public Texture texture;
	
	public Bow(Vector2 pos, float width, float height) {
		super(new Texture(Gdx.files.internal("data/bow.png")));
		this.pos = pos;
		this.width = width;
		this.height = height;
	}
	
	public void render(SpriteBatch batch) {
		 batch.draw(this.getTexture(), pos.x, pos.y, getOriginX(), getOriginY(), width, height, getScaleX(), getScaleY(), getRotation(),
	                getRegionX(), getRegionY(), getRegionWidth(), getRegionHeight(), false, false);
	}
}
