@echo off
echo Calculating..
SET count=1
FOR /f "tokens=*" %%G IN ('dir "%CD%\desktop\src\com\robocos\simulation\*.java" /b /s') DO (type "%%G") >> lines.txt
FOR /f "tokens=*" %%G IN ('dir "%CD%\core\src\com\robocos\simulation\*.java" /b /s') DO (type "%%G") >> lines.txt
SET count=1
FOR /f "tokens=*" %%G IN ('type lines.txt') DO (set /a lines+=1)
echo This project currently has %lines% lines of code. 
del lines.txt
PAUSE